const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

function html(color) {
  let contentHTML = "";
  color.forEach((item) => {
    var contentTR = "";
    contentTR = `<button class="color-button ${item}"></button>`;
    contentHTML += contentTR;
  });

  document.getElementById("colorContainer").innerHTML = contentHTML;
}

html(colorList);
